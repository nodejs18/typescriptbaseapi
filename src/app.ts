import express, {Application} from 'express'
import "reflect-metadata"

export class App {

    private app:Application = express()
    let a:

    constructor(){
       this.setConfiguration()
       this.setMiddlewares()
       this.setRoutes() 
    }
    
    setConfiguration = () => {
        
    }

    setMiddlewares = () => {

    }

    setRoutes = () => {

    }

    runApplication = () => {
        this.app.listen(2000)
        console.log('Server on port 3000')
    } 

}
