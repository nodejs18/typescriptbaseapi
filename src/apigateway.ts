import {App} from './app'

const main = async () => {
    const app:App = new App()
    await app.runApplication()
}

main()